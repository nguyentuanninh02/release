﻿using System.Runtime.CompilerServices;

static string GetArticleSummary(string content, int maxLength)
{
    if (content.Length > maxLength)
    {
        if (content[maxLength].Equals(" "))
        {
            return content.Substring(0, maxLength) + "...";
        }
        else
        {
            int index = 0;
            for (int i = maxLength; i >= 0; --i)
            {
                if (content[i].Equals(' '))
                {
                    index = i;
                    break;
                }
            }
            if (index > 0)
                return content.Substring(0, index) + "...";
            else 
                return content.Substring(0, maxLength) + "...";
        }

    }
    else return content;
}

Console.WriteLine("Content of article: ");
string a = Console.ReadLine();
Console.WriteLine("Max length of summary");
int length = int.Parse(Console.ReadLine());
Console.WriteLine(GetArticleSummary(a, length));