﻿using P3;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem03
{
    internal class Service
    {
        List<Student> listStudents = new List<Student>();
        Validate validate = new Validate();

        public void AddStudent()
        {
            while (true)
            {
                int maSV;
                while (true)
                {
                    maSV = validate.InputInt("Student Id: ", 1, 100000);
                    if (validate.CheckIdExist(listStudents, maSV))
                    {
                        break;
                    }
                }
                string name = validate.InputString("Student Name: ", "[A-Za-z\\s]+");
                DateTime startDate = validate.InputDate("Start Date: ");
                decimal sqlMark = validate.InputDecimal("Sql Mark: ", 0, 10);
                decimal CsharpMark = validate.InputDecimal("C# Mark: ", 0, 10);
                decimal DsaMark = validate.InputDecimal("Dsa Mark: ", 0, 10);
                Student student1 = new Student(maSV, name, startDate, sqlMark, CsharpMark, DsaMark);
                student1.Graduate();
                listStudents.Add(student1);

                Console.WriteLine("Bạn có muốn nhập tiếp không: ");
                if (!validate.CheckInputYN())
                {
                    return;
                }
            }
        }
        public void ShowStudent()
        {
            foreach (Student student in listStudents)
            {
                Console.WriteLine(student.GetCertificate());
            }
        }

        public void Init()
        {
            Student student1 = new Student(1, "Ninh", DateTime.Now, 8, 8, 10);
            student1.Graduate();
            listStudents.Add(student1);

        }
    }
}
