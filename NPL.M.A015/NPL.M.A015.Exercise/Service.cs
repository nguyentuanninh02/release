﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.Intrinsics.Arm;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Service
    {
        List<Department> departments = new List<Department>
        {
            new Department(1, "CNTT"),
            new Department(2, "Tai chinh"),
            new Department(3, "Kinh doanh"),
            new Department(4, "Nhan su")
        };
        List<Employee> employees = new List<Employee>
        {
            new Employee(1, "Ninh", 21, "Hai Duong", DateTime.Now, false, 1),
            new Employee(2, "An", 49, "Hai Duong", DateTime.Now, true, 1),
            new Employee(3, "Phuong", 48, "Hai Duong", DateTime.Now, true, 2),
            new Employee(4, "Binh", 14, "Hai Duong", DateTime.Now, true, 3),
        };
        List<ProgramingLanguage> programingLanguages = new List<ProgramingLanguage>
        {
            new ProgramingLanguage("C++"),
            new ProgramingLanguage("Java"),
            new ProgramingLanguage("Python"),
            new ProgramingLanguage("Csharp")
        };
        List<LanguageEmployee> languageEmployees = new List<LanguageEmployee>()
        {
            new LanguageEmployee("C++", 1),
            new LanguageEmployee("C++", 2),
            new LanguageEmployee("C++", 3),
            new LanguageEmployee("Java", 2),
            new LanguageEmployee("Python", 2),
        };

        public List<Department> GetDepartments(int numberOfEmployees)
        {
            var result = from dp in departments
                         join ep in employees on dp.DeparmentId equals ep.DepartmentId into Gr
                         where Gr.Count() >= numberOfEmployees
                         select dp;
            return result.ToList();
        }

        public List<Employee> GetEmployeesWorking()
        {
            return employees = employees.Where(x => x.Status == true).ToList();
        }

        public List<Employee> GetEmployees(string languageName)
        {
            var emp = from employee in employees
                      join lep in languageEmployees on employee.EmployeeId equals lep.EmloyeeId
                      where lep.LanguageName == languageName
                      select employee;
            return emp.ToList();
        }
        public List<ProgramingLanguage> GetLanguages(int employeeId)
        {
            var result = from plg in programingLanguages
                         join lep in languageEmployees on plg.LanguageName equals lep.LanguageName
                         where lep.EmloyeeId == employeeId
                         select plg;
            return result.ToList<ProgramingLanguage>();
        }

        public List<Employee> GetSeniorEmployee()
        {
            List<Employee> result = new List<Employee>();
            var listLg = from emp in employees
                         join lep in languageEmployees on emp.EmployeeId equals lep.EmloyeeId into Gr
                         where Gr.Count() > 1
                         select emp;
            return listLg.ToList();
        }

        //lấy ra những nhân viên có tên là tên nhập vào, sắp xếp từ lớn đến bé hoặc từ bé đến lớn
        // sau đó phân trang
        public List<Employee> GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            List<Employee> result = new List<Employee>();
            var listEp = from ep in employees
                         select ep;
            if (employeeName != "")
            {
                listEp = from ep in employees
                         where ep.EmployeeName.Contains(employeeName)
                         select ep;
            }
            else
            {
                listEp = from ep in employees
                         select ep;
            }

            if (order.Equals("ASC"))
            {
                listEp = listEp.OrderByDescending(x => x.EmployeeId);
            }
            else if (order.Equals("DESC"))
            {
                listEp = listEp.OrderByDescending(x => x.EmployeeId).Reverse();
            }
            result= listEp.ToList();
            int startIndex = (pageIndex - 1) * pageSize;
            int endIndex = startIndex + pageSize < result.Count ? startIndex + pageSize : result.Count - 1;
            List<Employee> final = new List<Employee>();
            for (int i = startIndex; i <= endIndex; ++i)
            {
                final.Add(result[i]);
            }
            return final;
        }

        public Dictionary<Department, List<Employee>> GetDepartments()
        {
            Dictionary<Department, List<Employee>> result = new Dictionary<Department, List<Employee>>();
            foreach (var dp in departments)
            {
                var listEp = from ep in employees
                             where ep.DepartmentId == dp.DeparmentId
                             select ep;
                result.Add(dp, listEp.ToList());
            }
            return result;
        }

        public void AddDepartment()
        {
            Department department = new Department();
            int id = 0;
            int countDp = 0;
            do
            {
                Console.WriteLine("Input department Id");
                id = int.Parse(Console.ReadLine());
                countDp = departments.Where(x => x.DeparmentId == id).Count();
            } while (countDp > 0);
            department.DeparmentId = id;
            Console.WriteLine("Input department name");
            department.DepartmentName = Console.ReadLine();
            departments.Add(department);

            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                AddDepartment();
            }
        }

        public void AddProgramingLanguage()
        {
            ProgramingLanguage programingLanguage = new ProgramingLanguage();
            string name = "";
            int countDp = 0;
            do
            {
                Console.WriteLine("Input programingLanguage name");
                name = Console.ReadLine();
                countDp = programingLanguages.Where(x => x.LanguageName.Equals(name)).Count();
            } while (countDp > 0);
            programingLanguage.LanguageName = name;
            programingLanguages.Add(programingLanguage);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                AddProgramingLanguage();
            }
        }

        public void AddEmployee()
        {
            Employee employee = new Employee();
            int id = 0;
            int countDp = 0;
            do
            {
                Console.WriteLine("Input employee Id");
                id = int.Parse(Console.ReadLine());
                countDp = employees.Where(x => x.EmployeeId == id).Count();
            } while (countDp > 0);
            employee.EmployeeId = id;

            Console.Write("Enter Employee Name: ");
            employee.EmployeeName = Console.ReadLine();

            Console.Write("Enter Age: ");
            employee.Age = int.Parse(Console.ReadLine());

            Console.Write("Enter Address: ");
            employee.Address = Console.ReadLine();

            Console.Write("Enter Hired Date (yyyy-MM-dd): ");
            employee.HiredDate = DateTime.Parse(Console.ReadLine());

            Console.Write("Enter Status (true/false): ");
            employee.Status = bool.Parse(Console.ReadLine());

            Console.Write("Enter Department ID: ");
            employee.DepartmentId = int.Parse(Console.ReadLine());
            employees.Add(employee);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                AddEmployee();
            }
        }
        public void AddLanguageEmployee()
        {
            string name = "";
            int countName = 0;
            do
            {
                Console.WriteLine("Input language name");
                name = Console.ReadLine();
                countName = programingLanguages.Where(x => x.LanguageName == name).Count();
            } while (countName == 0);

            int id = 0;
            int countId = 0;
            do
            {
                Console.WriteLine("Input employee Id");
                id = int.Parse(Console.ReadLine());
                countId = employees.Where(x => x.EmployeeId == id).Count();
            } while (countId == 0);
            LanguageEmployee languageEmployee = new LanguageEmployee(name, id);
            if (languageEmployees.Where(x => (x.LanguageName.Equals(name) && (x.EmloyeeId == id))).Count() > 0)
            {
                Console.WriteLine("Really exsit");
            }
            else
            {
                Console.WriteLine("Add success");
                languageEmployees.Add(languageEmployee);
            }
        }
    }
}
