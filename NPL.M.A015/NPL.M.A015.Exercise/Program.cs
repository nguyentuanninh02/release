﻿using NPL.M.A015.Exercise;
using System.ComponentModel.DataAnnotations;
using System.Net;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.InputEncoding = System.Text.Encoding.Unicode;
Service service = new Service();
bool exit = false;

while (!exit)
{
    Console.WriteLine("=======================================================");
    Console.WriteLine("Select an option:");
    Console.WriteLine("1. Get Departments with at least X Employees");
    Console.WriteLine("2. Get All Employees");
    Console.WriteLine("3. Get Employees by Language");
    Console.WriteLine("4. Get Languages of an Employee");
    Console.WriteLine("5. Get Senior Employees");
    Console.WriteLine("6. Get Employees with Paging");
    Console.WriteLine("7. Get All Departments");
    Console.WriteLine("8. Add Department");
    Console.WriteLine("9. Add Employee");
    Console.WriteLine("10. Add Language for Employee");
    Console.WriteLine("11. Add Programming Language");
    Console.WriteLine("12. Exit");

    string choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            Console.Write("Enter the minimum number of employees: ");
            if (int.TryParse(Console.ReadLine(), out int minEmployees))
            {
                List<Department> list1 = service.GetDepartments(minEmployees);
                Console.WriteLine("List department: ");
                foreach (Department department in list1)
                {
                    Console.WriteLine(department.DeparmentId + ": " + department.DepartmentName);
                }
            }
            else
            {
                Console.WriteLine("Invalid input. Please enter a valid number.");
            }
            break;
        case "2":
            List<Employee> list = service.GetEmployeesWorking();
            Console.WriteLine("Employees are working: ");
            Console.WriteLine(string.Format("{0, -5} {1, -15} {2, -5} {3, -15} {4, -15} {5, -15} {6, -5}", "Id", "Employee Name", "Age", "Address", "HiredDate", "Status", "Department Id"));
            foreach (Employee employee in list)
            {
                Console.WriteLine(employee.ToString());
            }
            break;
        case "3":
            Console.Write("Enter the language name: ");
            string languageName = Console.ReadLine();
            List<Employee> list3 = service.GetEmployees(languageName);
            Console.WriteLine("List all employees know language Name");
            Console.WriteLine(string.Format("{0, -5} {1, -15} {2, -5} {3, -15} {4, -15} {5, -15} {6, -5}", "Id", "Employee Name", "Age", "Address", "HiredDate", "Status", "Department Id"));
            foreach (Employee employee in list3)
            {
                Console.WriteLine(employee.ToString());
            }
            break;
        case "4":
            Console.Write("Enter the employee ID: ");
            if (int.TryParse(Console.ReadLine(), out int employeeId))
            {
                List<ProgramingLanguage> list4 = service.GetLanguages(employeeId);
                Console.Write("programing languages which employee has employeeId know: ");
                foreach (ProgramingLanguage employee in list4)
                {
                    Console.Write(employee.LanguageName+" ");
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Invalid input. Please enter a valid number.");
            }
            break;
        case "5":
            List<Employee> list5 = service.GetSeniorEmployee();
            Console.WriteLine("Senior Employees: ");
            Console.WriteLine(string.Format("{0, -5} {1, -15} {2, -5} {3, -15} {4, -15} {5, -15} {6, -5}", "Id", "Employee Name", "Age", "Address", "HiredDate", "Status", "Department Id"));
            foreach (Employee employee in list5)
            {
                Console.WriteLine(employee.ToString());
            }
            break;
        case "6":
            Console.Write("Enter page index: ");
            if (int.TryParse(Console.ReadLine(), out int pageIndex))
            {
                if(pageIndex < 1)
                {
                    Console.WriteLine("Page index must greater than 1");
                    break;
                }
                Console.Write("Enter page size: ");
                if (int.TryParse(Console.ReadLine(), out int pageSize))
                {
                    Console.Write("Enter employee name (or leave empty): ");
                    string employeeName = Console.ReadLine();
                    Console.Write("Enter order (ASC or DESC): ");
                    string order = Console.ReadLine();
                    List<Employee> list6= service.GetEmployeePaging(pageIndex, pageSize, employeeName, order);
                    Console.WriteLine("List Employee: ");
                    Console.WriteLine(string.Format("{0, -5} {1, -15} {2, -5} {3, -15} {4, -15} {5, -15} {6, -5}", "Id", "Employee Name", "Age", "Address", "HiredDate", "Status", "Department Id"));
                    foreach (Employee employee in list6)
                    {
                        Console.WriteLine(employee.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input for page size. Please enter a valid number.");
                }
            }
            else
            {
                Console.WriteLine("Invalid input for page index. Please enter a valid number.");
            }
            break;
        case "7":
            Dictionary<Department, List<Employee>> result = service.GetDepartments();
            foreach(var dp in result)
            {
                Console.Write("Department "+ dp.Key.DepartmentName+": ");
                foreach(var ep in dp.Value)
                {
                    Console.Write(ep.EmployeeName+" ");
                }
                Console.WriteLine();
            }
            break;
        case "8":
            service.AddDepartment();
            break;
        case "9":
            service.AddEmployee();
            break;
        case "10":
            service.AddLanguageEmployee();
            break;
        case "11":
            service.AddProgramingLanguage();
            break;
        case "12":
            exit = true;
            break;
        default:
            Console.WriteLine("Invalid choice. Please select a valid option.");
            break;
    }
}

