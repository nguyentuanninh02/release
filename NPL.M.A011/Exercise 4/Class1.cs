﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_4
{
    internal static class Class1
    {
        public static int ElementOfOrder2(int[] arr)
        {
            int count = 0;
            int maxValue = int.MinValue;
            int maxValue2 = int.MinValue;
            for (int i = 0; i < arr.Length; ++i)
            {
                if (arr[i] > maxValue)
                {
                    count++;
                    maxValue2 = maxValue;
                    maxValue = arr[i];
                }
            }
            if (count < 2)
            {
                return maxValue;
            }
            return maxValue2;
        }

        public static T ElementOfOrder<T>(this T[] array, int orderLargest) where T: IComparable<T>
        {
            T[] array1 = array.Distinct().ToArray();
            for (int i= 0; i< array1.Length; ++i)
            {
                for(int j= i; j< array1.Length; ++j)
                {
                    if (array1[i].CompareTo(array1[j])> 0)
                    {
                        (array1[i], array1[j]) = (array1[j], array1[i]);
                    }
                }
            }
            if (orderLargest < array1.Length)
            {
                return array1[array1.Length - orderLargest];

            }
            else throw new Exception();
        }
    }
}
