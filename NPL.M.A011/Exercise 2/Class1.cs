﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_2
{
    internal static class Class1
    {
        public static int[] RemoveDuplicate(int[] arr)
        {
            if (arr == null || arr.Length == 0)
            {
                throw new InvalidOperationException("Lỗi");
            };
            return arr.Distinct().ToArray();
        }

        public static T[] RemoveDuplicate<T>(T[] arr)
        {
            List<T> list = new List<T>();
            foreach(T a in arr)
            {
                if (!list.Contains(a))
                {
                    list.Add(a);
                }
            }
            return list.ToArray();
        }
    }
}
