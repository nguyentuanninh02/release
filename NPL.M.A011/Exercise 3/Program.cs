﻿int LastIndexOf<T>(T[] array, T elementValue)
{
    int index = -1;
    for(int i = 0; i < array.Length; i++)
    {
        if (array[i].Equals(elementValue))
        {
            index = i; 
        }
    }
    return index;
}

int[] array = { 1, 2, 3, 5, 7, 3, 2 };
Console.WriteLine(LastIndexOf(array, 3));

string[] arrString = { "Hoang", "Trong", "A", "Hoang", "Trong", "Hieu" };
Console.WriteLine(LastIndexOf(arrString,"Trong" ));