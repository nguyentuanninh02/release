﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise
{
    public static class Class1
    {
        public static int CountInt(ArrayList array)
        {
            int count = 0;
            foreach (var o in array)
            {
                if (o is Int32)
                {
                    count++;
                }
            }
            return count;
        }

        public static int CountOf(ArrayList array, Type dataType)
        {
            int count = 0;
            foreach (var o in array)
            {
                if (o != null && o.GetType() == dataType)
                {
                    count++;
                }
            }
            return count;
        }

        public static int CountOf<T>(ArrayList array)
        {
            int count = 0;
            foreach (var o in array)
            {
                if (o != null && o is T)
                {
                    count++;
                }
            }
            return count;
        }
        private static bool IsNumericType(Type type)
        {
            return type.IsPrimitive && type!= typeof(bool);
        }
        public static T MaxOf<T>(this ArrayList array) where T : IComparable<T>
        {
            if (IsNumericType(typeof(T)))
            {
                T max;
                bool check = false;
                for (int i = 0; i < array.Count; i++)
                {
                    if (array[i].GetType() == typeof(T))
                    {
                        check = true;
                        max = (T)array[i];
                        foreach (var item in array)
                        {
                            if (item is T tItem)
                            {
                                if (tItem.CompareTo(max) > 0)
                                {
                                    max = tItem;
                                }
                            }
                        }
                        return max;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("T must be a numeric data type.");
            }
            return (T)array[0];
        }
    }
}
